﻿#include <iostream>
#include <time.h>

#define N 5

int main()
{
    // Creating and displaying array
    int arr[N][N];

    for (int i(0); i < N; i++) 
    {
        for (int j(0); j < N; j++) 
        {
            arr[i][j] = i + j;
            std::cout << arr[i][j] << " ";
        }
        std::cout << "\n";
    }

    std::cout << "\n";

    // Getting date info
    struct tm buf;
    time_t t = time(NULL);
    localtime_s(&buf, &t);

    // Getting row number
    int row = buf.tm_mon % N;


    // Getting row sum
    int sum = 0;

    for (int i(0); i < N; i++) 
    {
        sum += arr[row][i];
    }

    // Displaying info (month and row increased for users to understand indexes)
    std::cout << "Month: " << buf.tm_mon + 1 << " | Row : " << row + 1 << " | Row sum: " << sum;

    return 0;
}
